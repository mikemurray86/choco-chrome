﻿$ErrorActionPreference = 'Stop';
$toolsDir   = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
$fileLocation = Join-Path $toolsDir 'ChromeSetup.msi'

$packageArgs = @{
  packageName   = $env:ChocolateyPackageName
  unzipLocation = $toolsDir
  fileType      = 'MSI'
  file          = $fileLocation
  softwareName  = 'Google Chrome*'
  checksum      = "$( Get-Content $toolsDir\checksum.sha256 )"
  checksumType  = 'sha256'
  silentArgs    = "/quiet /norestart /l*v `"$($env:TEMP)\$($env:chocolateyPackageName).$($env:chocolateyPackageVersion).MsiInstall.log`""
  validExitCodes= @(0)
}

Install-ChocolateyInstallPackage @packageArgs
Set-Service -Name gupdate -Startuptype Disabled
Set-Service -Name gupdatem -Startuptype Disabled
Stop-Service -Name gupdate
Stop-Service -Name gupdatem
